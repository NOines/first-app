# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'a1cfaf5668595f102c5dedd17908911e16bacab16511ac2e2bc81bcf95ca6e8aa52acde062981f852b6afeae2ad617a593e8626c24ba7d4f2d07a83e5cf57185'
